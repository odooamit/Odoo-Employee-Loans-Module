# -*- coding : utf-8 -*-

{
    'name': 'Employee Loans',
    'version': '1.0',

    'author': 'Obay Abdelgadir',
    'description': """
Manage Employee Loans
=====================
Credits
This application uses Open Source components. You can find the source code of their open source projects along with license information below. We acknowledge and are grateful to these developers'
for their contributions to open source.\n

Project: HR Payroll https://github.com/odoo/odoo.git#saas-7\n
Copyright (c) 2004-2015 Odoo S.A.\n
License (AGPL-3) https://www.gnu.org/licenses/agpl-3.0.en.html\n
""",

    'data': [
        'security/ir.model.access.csv',
        'security/loan_security.xml',

        'sequences/loan_sequence.xml',

        'wizards/loan_refusal_wizard.xml',

        'views/company.xml',
        'views/employee.xml',
        'views/loan.xml',
        'views/loan_accounting.xml',
        'views/payslip.xml',
        'views/payslip_line.xml',
        'views/salary_rule.xml',
        'views/salary_structure.xml',
        'views/loan_installment.xml',
        # 'workflows/workflow.xml',
    ],
    'installable': True,
    'application': True,
}
