#-*- coding:utf-8 -*-
from openerp import fields, api, models


class SalaryRule(models.Model):
    _inherit = 'hr.salary.rule'

    loan_inputs = fields.One2many('employee.loan.payslip.input', 'rule_id', readonly=True)
    amount_select = fields.Selection(selection_add=[('loan', 'Loan Rule')])

    @api.multi
    def compute_rule(self, localdict):
        # this function is called on a loop of contracts.
        # and the current contract is included in the localdict parameter.
        self.ensure_one()
        if self.amount_select != 'loan':
            return super(SalaryRule, self).compute_rule(self.id, localdict)

        payslip = self.env['hr.payslip'].browse([localdict['payslip'].id])  # values of localdict are of custom types.
        current_contract = self.env['hr.contract'].browse([localdict['contract'].id])
        loan_inputs = payslip.loan_input_line_ids.filtered(lambda r: r.loan_contract_id == current_contract)
        if not loan_inputs:
            return 0, 1, 100.0

        amount = sum(loan_inputs.mapped('amount'))
        line = payslip.details_by_salary_rule_category.filtered(lambda r: r.rule_id.id == self.id)
        return amount, 1, 100.0

    @api.constrains('amount_select')
    def _validate_one_loan_rule_per_structure(self):
        for r in self:
            if r.amount_select != 'loan':
                continue
            structs = r.env['hr.payroll.structure'].search([])
            for struct in structs:
                if r in struct.rule_ids:
                    struct.validate_loan_rules()


class PayslipLine(models.Model):
    _inherit = 'hr.payslip.line'

    amount_select = fields.Selection(selection_add=[('loan', 'Loan Rule')])
