from openerp import fields, api, models, _
from openerp.exceptions import UserError, AccessError, ValidationError, Warning
from datetime import datetime, date
from dateutil.relativedelta import relativedelta
import calendar
from math import ceil, floor


class EmployeeLoan(models.Model):
    _name = 'employee.loan'
    _description = 'Loan Request'
    _inherit = ['mail.thread']

    @api.depends('amount', 'installment_ids.amount_paid')
    def _compute_balance(self):
        for r in self:
            if r.amount == 0:
                r.state = 'draft'
                continue

            r.amount_paid = sum(r.installment_ids.mapped('amount_paid'))
            r.balance = r.amount - r.amount_paid

            if r.balance < 0:
                raise ValidationError('Loan "{}" balance can not be less than 0.\n'
                                      'The Amount Paid is larger than the Amount.'.format(r.name))
            else:
                r.payslip_status = False

    @api.depends('employee_id')
    def _compute_get_total_running(self):
        self.running_loan_count=self.env['employee.loan'].search_count([('employee_id','=',self.employee_id.id),('state','=','running')])


    def _default_employee(self):
        employee = self.env['hr.employee']
        employee_id = employee.search([('user_id', '=', self.env.user.id)])
        if employee_id:
            return employee_id[0]
        else:
            raise UserError("You are not registered as an employee!\nPlease contact system admin.")

    def _default_date(self):
        return fields.Date.today()

    def _default_company(self):
        return self. _default_employee().company_id



    def _default_currency(self):
        return self._default_employee().company_id.currency_id

    name = fields.Char('Name', required=True, default='New', readonly=True)
    description = fields.Char('Description', required=True, readonly=True,
                              states={'draft': [('readonly', False)]}, defaut="Test")
    amount = fields.Monetary('Amount', currency_field='currency_id', required=True, readonly=True,
                             states={'draft': [('readonly', False)]})
    amount_paid = fields.Monetary('Paid', currency_field='currency_id', readonly=True, compute=_compute_balance,
                                  store=True)
    balance = fields.Monetary('Balance', currency_field='currency_id', compute=_compute_balance, store=True)
    type = fields.Selection([
        ('short', 'Short'),
        ('long', 'Long')], required=True, string='Type', default='short', readonly=True,
        states={'draft': [('readonly', False)]})
    type_help = fields.Char('Type Explanation', compute='_compute_type_help')
    payslip_status = fields.Boolean('Deducted?', readonly=True, compute=_compute_balance, store=True)
    date = fields.Date('Date', default=_default_date, required=True, readonly=True, states={'draft': [('readonly', False)]})
    months = fields.Integer('Number of Months', readonly=True, states={'draft': [('readonly', False)]})
    employee_id = fields.Many2one('hr.employee', 'Employee', default=_default_employee, readonly=True,
                                  states={'draft': [('readonly', False)]})
    # max_loan = fields.Monetary(related='employee_id.company_id.loan_max_amount', readonly=True)
    max_loan = fields.Monetary(related='company_id.loan_max_amount', readonly=True)
    contract_id = fields.Many2one('hr.contract', 'Contract', readonly=True,
                                  states={'confirmed': [('readonly', False), ('required', True)]})
    rule_id = fields.Many2one(related='contract_id.struct_id.loan_rule_id', string='Salary Rule', readonly=True,
                              states={'admin_approved': [('required', True)]})
    analytic_account = fields.Many2one(related='rule_id.analytic_account_id',string='Analytic Account', readonly=True)
    journal_from_id = fields.Many2one('account.journal', 'To be Paid From', readonly=True,
                                      states={'admin_approved': [('readonly', False), ('required', True)]},
                                      domain="[('type', 'in', ('cash', 'bank')), ('company_id', '=', company_id)]")
    move_id = fields.Many2one('account.move', 'Journal Entry', readonly=True)
    installment_ids = fields.One2many('employee.loan.installment', 'loan_id', readonly=True,
                                      states={'confirmed': [('readonly', False), ('required', True)]})
    payment_ids = fields.Many2many('account.payment', string='Payments', readonly=True)

    company_id = fields.Many2one('res.company', 'Company', required=True, readonly=True,
                                 states={'draft': [('readonly', False)]}, default=_default_company)
    currency_id = fields.Many2one(related='company_id.currency_id', string="Company's Currency", required=True,
                                  readonly=True)
    start_deductions = fields.Boolean('Start Deductions?', compute='_compute_running_state', store=True)
    fully_paid_to_employee = fields.Boolean('Fully Paid to Employee', compute='_compute_running_state', store=True)
    # why currncy_id is related?
    # because it is dependent on the company so that the currency in the payroll match with this.
    running_loan_count=fields.Integer(compute='_compute_get_total_running')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('submitted', 'Submitted'),
        ('confirmed', 'Confirmed'),
        ('ceo_approve', 'Waiting CEO Approve'),
        ('admin_approved', 'Admin Approved'),
        ('f_approved', 'Finance Approved'),
        ('running', 'Running'),
        ('closed', 'Closed'),
        ('canceled', 'Refused'),
    ], 'Status', select=True, default='draft', compute='_compute_running_state', store=True)

    @api.depends('type')
    def _compute_type_help(self):
        for r in self:
            if r.type == 'short':
                r.type_help = '(One Time Deduction)'
            if r.type == 'long':
                r.type_help = '(Deduction by Installments)'

    @api.multi
    def _compute_payment_method_id(self):
        for r in self:
            r.payment_method_id = self.env.ref('account.account_payment_method_manual_out')

    @api.multi
    def is_ceo_required(self):
        self.ensure_one()
        maximum = self.contract_id.wage + self.contract_id.assignment
        if self.amount <= maximum:
            return False
        else:
            return True

    @api.constrains('amount')
    def _validate_amount(self):
        for r in self:
            if r.amount == 0:
                raise ValidationError('Sorry "Amount" can not be zero!')
            if r.state not in ['draft']:
                continue
            r._validate_months()

            if r.amount < 0:
                raise ValidationError('Sorry "Amount" can not be negative!')

            contract_ids = r.sudo().get_contracts(r.date, r.date)
            contracts = self.sudo().env['hr.contract'].browse(contract_ids)
            wage = 0.0

            for contract in contracts:
                # get max wage
                if contract.wage > wage:
                    wage = contract.wage

            if wage == 0:
                raise ValidationError('Sorry, your contract has not been configured.\n'
                                      'Please contact HR department to check your contract.')
            if r.type == 'long':
                amount = max(r.get_installment_amounts())
                max_legal_installment = r.get_deductible_installment(wage)
                if amount > max_legal_installment:
                    minimum_months_suggestion = int(ceil(r.amount/max_legal_installment))
                    while True:
                        if max(self.get_installment_amounts(minimum_months_suggestion)) > max_legal_installment:
                            minimum_months_suggestion += 1
                        else:
                            break

                    raise ValidationError('Sorry, Installments are larger than the approved deduction percent of your wage.'
                                          '\nPleas increase the Number of Months to {} or more'.
                                          format(minimum_months_suggestion))
            else:
                max_amount = r.get_deductible_installment(wage)
                if r.amount > max_amount:
                    raise ValidationError('Sorry, Loan amount is larger than the allowed percentage.\n'
                                          'Maximum amount is {}.'.format(max_amount))

    @api.constrains('contract_id')
    def validate_contract(self):
        for r in self:
            if r.contract_id.employee_id.id != r.employee_id.id:
                raise ValidationError('The selected contract does not belong to the employee!')

    @api.constrains('employee_id')
    def validate_employee(self):
        for r in self:
            if not r.employee_id.sudo().company_id:
                raise ValidationError("{}'s company is not set".format(r.employee_id.name))

            uid = self.env.user.id
            if not r.employee_id:
                continue
            if uid != r.employee_id.user_id.id:
                raise ValidationError('Sorry, you can not request loans for {}.'.
                                      format(r.employee_id.name))

    @api.constrains('rule_id')
    def validate_rule(self):
        for r in self:
            if r.contract_id and r.rule_id not in r.contract_id.struct_id.rule_ids:
                raise ValidationError('The selected salary rule does not belong to the salary structure of the contract!')

    @api.constrains('months')
    def _validate_months(self):
        for r in self:
            if r.type == 'long' and r.months < 2:
                raise ValidationError('Sorry "Number of Months" can not be less than 2.')

    @api.constrains('journal_from_id')
    def _validate_journal_from(self):
        for r in self:
            if r.journal_from_id and r.journal_from_id.company_id != r.company_id:
                raise ValidationError('Journal "To be Paid From" is from a different company!\nLoan: {}'.format(r.name))

    @api.multi
    def _validate_installments(self):
        for r in self:
            total = sum(r.installment_ids.mapped('amount')) or 0.0
            if total != r.amount:
                operator = 'less' if total < r.amount else 'more'
                raise ValidationError('Installments total ({:.2f}) is {} than the loan ({:.2f})'.
                                      format(total, operator, r.amount))

    @api.multi
    def get_installment_amounts(self, months=False):
        self.ensure_one()
        months = months and months or self.months
        batch = self.amount/months
        int_batch = int(batch/10)*10
        res = [int_batch]*months

        diff = self.amount - (int_batch * months)

        number_of_tens = int(floor(diff/10))
        if number_of_tens >= 1:
            for x in xrange(number_of_tens):
                res[x] += 10
            res[0] += self.amount - sum(res)
        else:
            res[0] += self.amount - sum(res)
        return res

    def check_payslip(self):
        all = self.env['hr.payslip'].search([('state','=','done'), ('employee_id', '=', self.employee_id.id)])
        if not all:
            return False
        last_payslip = all[len(all)-1]
        if fields.Date.from_string( last_payslip.date_to) <= fields.Date.from_string(self.date):
            return False
        else:
            return True


    @api.multi
    def calc_installments(self):
        self.ensure_one()
        installment_length = self.type == 'long' and self.months or 1
        amounts = installment_length > 1 and self.get_installment_amounts() or [self.amount]
        loan_date = fields.Date.from_string(self.date)  if not self.sudo().check_payslip() else  fields.Date.from_string(self.date) + relativedelta(months=+1)

        installments = []
        count = 1
        for i in xrange(installment_length):
            if amounts[i] == 0:
                continue

            # last_day = calendar.monthrange(loan_date.year, loan_date.month)
            # loan_date = date(loan_date.year, loan_date.month, last_day[1])

            installment = {
                'name': str(self.name) + '/' + str(count),
                'amount': amounts[i],
                'date': loan_date,
                'payslip_status': False,
            }
            loan_date += relativedelta(months=+1)
            count += 1
            if not isinstance(self.id, models.NewId):
                installment['loan_id'] = self.id
            installments.append(installment)
        return installments

    @api.multi
    def create_installments(self):
        self.ensure_one()
        self.installment_ids.unlink()
        for data_dict in self.calc_installments():
            self.installment_ids.create(data_dict)

    @api.multi
    def create_installments_in_cache(self):
        for data_dice in self.calc_installments():
            self. installment_ids += self.installment_ids.new(data_dice)

    @api.onchange('months', 'amount')
    def _onchange_months_amount(self):
        if self.type == 'short' or self.months < 2 or self.amount == 0:
            return
        self.installment_ids = False
        self.create_installments_in_cache()
        self.months = len(self.installment_ids)

    @api.onchange('type')
    def _onchange_type(self):
        for r in self:
            if r.type == 'long':
                r.months = r.months or 2

    @api.multi
    def set_installments(self):
        for r in self:
            r.create_installments()

    def get_contracts(self, date_from, date_to = False):
        if not self.employee_id:
            return False
        if not date_to:
            date_to = date_from
        slip_obj = self.env['hr.payslip']
        contracts = slip_obj.get_contract(self.employee_id, date_from, date_to)
        return contracts

    # STATE BUTTONS
    @api.multi
    def draft_loan(self):
        for r in self:
            if not r.employee_id.user_id == self.env.user:
                raise AccessError("Sorry, you can not set this loan to draft.")
        self.sudo().state='draft'

    @api.multi
    def submit_loan(self):
        for r in self:
            contracts = r.sudo().get_contracts(self.date, self.date)
            r.sudo().contract_id = contracts and contracts[0] or False

            hr_manager_group = ['base.group_hr_manager']
            message = {
                'subject': "Loan Validation",
                'body': _("There is a Loan with number {} waiting for your validation.".format(r.name)),
                'type': "notification",
                'subtype': "mt_comment",
            }
            thread = self.env['mail.thread']
            thread.message_post_by_company(r.company_id, hr_manager_group, **message)
            message['body'] = _("Waiting for admin confirmation.")
            r.message_post(**message)
            r.state = 'confirmed'  # make sure to write the state at last to confirm record rules

    @api.multi
    def get_deductible_installment(self, wage):
        self.ensure_one()
        res = 0.0

        if self.type == 'long':
            max_legal_installment_percent = self.employee_id.sudo().company_id.loan_installment_max_percentage_from_wage
            if max_legal_installment_percent == 0.0:
                raise ValidationError('Sorry, the approved deduction percent for loan installments is not set in your'
                                      'company.')

            res = (max_legal_installment_percent / 100) * wage

        elif self.type == 'short':
            max_short_loan_percent = self.employee_id.sudo().company_id.short_loan_max_percentage_from_wage
            if not max_short_loan_percent:
                raise ValidationError(
                    'Sorry, the approved short loan percent for is not configured in your company.')
            res = (max_short_loan_percent / 100) * wage
        return res

    @api.multi
    def admin_approve_loan(self):
        if not self.env.user.has_group('base.group_hr_manager'):
            raise AccessError("Sorry, you do not have permission to approve this loan.")

        self._validate_installments()
        self.installment_ids.validate_date()
        for r in self:
            if r.type == 'long':
                max_installment = max(r.installment_ids.mapped('amount'))
                legal_installment_amount = r.get_deductible_installment(r.contract_id.wage)
                if max_installment > legal_installment_amount:
                    raise ValidationError('Sorry, Installment ({}) is larger than the approved deduction percent of '
                                          'the wage({}).\n'.format(max_installment, legal_installment_amount))

            if r.is_ceo_required():
                ceo_group = ['common_groups.group_hr_ceo']

                message = {
                    'subject': "Employee Loan Approval",
                    'body': _("There is a Loan with number {} waiting for your approval.".format(r.name)),
                    'type':"notification",
                    'subtype':"mt_comment",
                }
                thread = self.env['mail.thread']
                thread.message_post_by_company(r.company_id, ceo_group, **message)
                message['body'] = _("Waiting for CEO approval.")
                r.message_post(**message)

                self.write({'state': 'ceo_approve'})  # make sure to write the state at last to confirm record rules
            else:
                adviser_group = ['account.group_account_manager']
                adviser_message = {
                    'subject': "Employee Loan Approval",
                    'body': _("There is a Loan with number {} waiting for your approval.".format(r.name)),
                    'type': "notification",
                    'subtype': "mt_comment",
                }
                thread = self.env['mail.thread']
                thread.message_post_by_company(r.company_id, adviser_group, **adviser_message)

                message = {
                    'subject': "Loan Validated",
                    'body': _("Your Loan {} has been confirmed by admin.").format(r.name),
                    'type':"notification",
                    'subtype':"mt_comment",
                }
                if r.employee_id.user_id.partner_id.id:
                    message['partner_ids'] = [(4, r.employee_id.user_id.partner_id.id)]
                    thread = self.env['mail.thread']
                    thread.message_post(**message)
                    message['partner_ids'] = []
                message['body'] = _("Confirmed")
                r.message_post(**message)

                self.write({'state': 'admin_approved'})  # make sure to write the state at last to confirm record rules

    @api.multi
    def ceo_approve_loan(self):
        if not self.env.user.has_group('common_groups.group_hr_ceo'):
            raise AccessError("Sorry, you do not have permission to approve this loan.")

        for r in self:
            adviser_group = ['account.group_account_manager']
            adviser_message = {
                'subject': "Employee Loan Approval",
                'body': _("There is a Loan with number {} waiting for your approval.".format(r.name)),
                'type': "notification",
                'subtype': "mt_comment",
            }
            thread = self.env['mail.thread']
            thread.message_post_by_company(r.company_id, adviser_group, **adviser_message)

            message = {
                'subject': "Loan Approved",
                'body': _("Your Loan {} has been approved by CEO.").format(r.name),
                'type': "notification",
                'subtype': "mt_comment",
            }
            if r.employee_id.user_id.partner_id.id:
                message['partner_ids'] = [(4, r.employee_id.user_id.partner_id.id)]
                thread = self.env['mail.thread']
                thread.message_post(**message)
                message['partner_ids'] = []
            message['body'] = _("Approved by CEO.")
            r.message_post(**message)

        self.write({'state': 'admin_approved'})  # make sure to write the state at last to confirm record rules

    @api.multi
    def finance_approve_loan(self):
        if not self.env.user.has_group('account.group_account_manager'):
            raise AccessError("Sorry, you do not have permission to approve this loan.")
        for r in self:
            if not r.employee_id.address_home_id:
                raise ValidationError('Please set the partner (Home Address) for {}'.format(r.employee_id.name))

            message = {
                'subject': "Loan Ready",
                'body': _("Congratulations, your Loan {} has been approved by finance and is now ready.").format(r.name),
                'type': "notification",
                'subtype': "mt_comment",
            }
            if r.employee_id.user_id.partner_id.id:
                message['partner_ids'] = [(4, r.employee_id.user_id.partner_id.id)]
                thread = self.env['mail.thread']
                thread.message_post(**message)
                message['partner_ids'] = []
            message['body'] = _("Approved.")
            r.message_post(**message)
        if not self.check_payslip():
            self.create_installments()
        self.write({'state': 'f_approved'})  # make sure to write the state at last to confirm record rules

    @api.depends('payment_ids.amount', 'payment_ids.state')
    def _compute_running_state(self):
        for r in self:
            posted_payments = r.payment_ids.filtered(lambda x: x.state == 'posted')
            if len(posted_payments) == 1:
                r._correct_installment_dates()

            paid_amount = sum(posted_payments.mapped('amount'))
            if paid_amount >= r.amount:
                r.state = 'running'
                r.fully_paid_to_employee = True
                r.start_deductions = True
            elif paid_amount > 0:
                r.state = 'f_approved'
                r.start_deductions = True
                r.fully_paid_to_employee = False
            elif r.payment_ids:
                r.state = 'f_approved'
                r.start_deductions = False
                r.fully_paid_to_employee = False
            else:
                r.state = 'draft'
                r.start_deductions = False
                r.fully_paid_to_employee = False

    @api.multi
    def open_payment_form(self):
        if not self.env.user.has_group('account.group_account_accountant'):
            raise AccessError("Sorry, you do not have permission to pay for this loan.")
        self.ensure_one()
        if not self.employee_id.address_home_id:
            raise ValidationError('Please set the partner (Home Address) for {}'.format(self.employee_id.name))

        self.payment_ids.filtered(lambda p: p.state == 'draft').unlink()

        remaining = self.amount - sum(self.payment_ids.filtered(lambda r: r.state == 'posted').mapped('amount'))

        payment = {
            'amount': remaining,
            'communication': 'Payment for loan #{}'.format(self.name),
            'origin': self.name,
            'payment_date': fields.Date.today(),
            'payment_type': 'outbound',
            'payment_method_id': self.env.ref('account.account_payment_method_manual_out').id,
            'journal_id': self.journal_from_id.id,
            'partner_id': self.employee_id.address_home_id.id,
            'company_id': self.company_id.id,
            'partner_type': 'employee'
        }
        payment = self.env['account.payment'].create(payment)
        self.payment_ids += payment

        return {
            'type': 'ir.actions.act_window',
            'res_model': 'account.payment',
            'res_id': payment.id,
            'view_mode': 'form',
            'view_type': 'form',
            'target': 'new',
            'context': {'loan_payment': True, 'loan_id': self.id},
            'view_id': self.env.ref('account.view_account_payment_form').id,
        }

    @api.multi
    def show_running_loan(self):
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'employee.loan',
            'view_mode': 'tree',
            'domain':[('state','=','running'),('employee_id.user_id','=',self.env.uid)],
            'view_id': self.env.ref('employee_loans.employee_loan_view_tree').id,
            'target': 'new',
        }

    @api.multi
    def _correct_installment_dates(self):
        self.ensure_one()
        dates = {}
        first_date = fields.Date.from_string(self.installment_ids[0].date)
        for i in self.installment_ids:
            dates[i.id] = fields.Date.from_string(i.date)
            if first_date > fields.Date.from_string(i.date):
                first_date = fields.Date.from_string(i.date)

        if not first_date < date.today():
            return

        diff = date.today() - first_date

        for i in self.installment_ids:
            if i.payslip_status:
                continue
            i_date = fields.Date.from_string(i.date)
            i.write({'date': i_date + diff})

    @api.multi
    def close_loan(self):
        for r in self:
            message = {
                'subject': "Loan Closed",
                'body': _("Your Loan {} has been closed (Totally paid).").format(r.name),
                'type':"notification",
                'subtype':"mt_comment",
            }
            if r.employee_id.user_id.partner_id.id:
                message['partner_ids'] = [(4, r.employee_id.user_id.partner_id.id)]
                thread = self.env['mail.thread']
                thread.message_post(**message)
                message['partner_ids'] = []
            message['body'] = _("Loan has been closed (Totally paid).")
            r.message_post(**message)
        self.write({'state': 'closed'})  # make sure to write the state at last to confirm record rules

    @api.multi
    def reject_loan(self, reason):
        states = self.mapped('state')
        if 'cancel' in states or 'draft' in states or 'f_approved' in states or 'running' in states or'closed' in states:
            raise ValidationError('Sorry, loan Request can not be Rejected!')

        for r in self:
            message = {
                'subject': "Loan Rejected",
                'body': "Your Loan {} has been refused.<br/><ul class=o_timeline_tracking_value_list><li>Reason<span>: </span><span class=o_timeline_tracking_value>{}</span></li></ul>".format(self.name, reason.replace('\n', '<br />')),
                'type':"notification",
                'subtype':"mt_comment",
            }
            if r.employee_id.user_id.partner_id.id:
                message['partner_ids'] = [(4, r.employee_id.user_id.partner_id.id)]
                thread = self.env['mail.thread']
                thread.message_post(**message)
                message['partner_ids'] = []
            message['body'] = _("Loan has been rejected.<br/><ul class=o_timeline_tracking_value_list><li>Reason<span>: </span><span class=o_timeline_tracking_value>{}</span></li></ul>".format(reason.replace('\n', '<br />')))
            r.message_post(**message)

        self.write({'state': 'canceled'})  # make sure to write the state at last to confirm record rules
    # STATE BUTTONS end

    @api.model
    def create(self, values):
        values['name'] = self.env['ir.sequence'].next_by_code('employee.loan')
        r = super(EmployeeLoan, self).create(values)
        r.create_installments()
        return r

    @api.multi
    def write(self, values):
        if self.state != 'draft':
            return super(EmployeeLoan, self).write(values)
        res = super(EmployeeLoan, self).write(values)
        self.sudo().create_installments()
        return res

    @api.multi
    def unlink(self):
        for r in self:
            if r.state != 'draft':
                raise UserError("Only draft loans can be deleted!")


class LoanInstallment(models.Model):
    _name = 'employee.loan.installment'
    _description = 'Loan Installment'

    def _default_date(self):
        loan = self.env['employee.loan'].browse([self.env.context.get('loan_id')])
        if not loan:
            return fields.Date.today()

        offset = len(loan.installment_ids) + 1

        i_date = fields.Date.from_string(fields.Date.today()) + relativedelta(months=offset)
        last_day = calendar.monthrange(i_date.year, i_date.month)
        return date(i_date.year, i_date.month, last_day[1])

    def _default_name(self):
        loan = self.env['employee.loan'].browse([self.env.context.get('loan_id')])
        if not loan:
            return ''
        return loan.name + '/' + str(len(loan.installment_ids)+1)

    def _default_amount(self):
        # this function is not critically important.
        # this function will not work properly unless the user save the changes in the installments first then press
        # the "Add an item" button in the installments
        loan = self.env['employee.loan'].browse([self.env.context.get('loan_id')])
        if not loan:
            return 0
        amount = loan.amount - sum(loan.installment_ids.mapped('amount'))
        return amount

    name = fields.Char('Reference', default=_default_name)
    amount = fields.Monetary('Amount', currency_field='currency_id', required=True, default=_default_amount)
    amount_paid = fields.Monetary('Paid', currency_field='currency_id', readonly=True)
    payslip_status = fields.Boolean('Deducted?', compute='_compute_payslip_status', store=True)
    date = fields.Date('Date', required=True, help='Specify the year and month for the loan', default=_default_date)
    loan_id = fields.Many2one('employee.loan', 'Loan', required=True, ondelete='cascade')
    employee_id = fields.Many2one(related='loan_id.employee_id', readonly=True)
    payslip_line = fields.Many2one('hr.payslip.line', string='Payslip Line', ondelete='restrict', readonly=True)
    is_balanced = fields.Boolean('Is Balanced', compute='_compute_is_balanced', store=True)
    currency_id = fields.Many2one(related='loan_id.currency_id', readonly=True, required=True)
    state = fields.Selection(related='loan_id.state')

    @api.depends('amount', 'amount_paid')
    def _compute_is_balanced(self):
        for r in self:
            r.is_balanced = r.amount == r.amount_paid

    @api.depends('amount_paid')
    def _compute_payslip_status(self):
        for r in self:
            r.payslip_status = r.amount_paid > 0

    @api.onchange('date')
    def _onchange_date(self):
        if not self.date:
            return
        i_date = fields.Date.from_string(self.date)
        last_day = calendar.monthrange(i_date.year, i_date.month)
        self.date = date(i_date.year, i_date.month, last_day[1])

    @api.constrains('amount', 'amount_paid')
    def _validate_balance(self):
        for r in self:
            if r.amount == 0:
                raise ValidationError('Sorry, installment amount can not be zero.')
            if r.amount < 0:
                raise ValidationError('Sorry, installment amount can not be negative ({}).'.format(r.amount))

            if r.amount_paid > r.amount:
                raise ValidationError('Installment paid amount ({}) can not be larger than the setup ({})'.format(
                    r.amount_paid, r.amount
                ))

    # @api.constrains('date') # this function (validate_date) as a constraint will not work properly,
    # for example if there was 2 installments one on month 8 and the other on 9, you
    # can not switch there months on the same transaction (the first to be written will
    # cause an exception).
    # so we call it from the employee.loan model in hr_approve().
    def validate_date(self):
        for r in self:
            obj_date = fields.Date.from_string(r.date)

            first_date = last_date = obj_date
            first_date += relativedelta(days=-(obj_date.day-1))
            last_day = calendar.monthrange(obj_date.year, obj_date.month)[1]
            last_date += relativedelta(days=last_day-obj_date.day)

            count = r.search_count([('date', '>=', first_date), ('date', '<=', last_date),
                                    ('loan_id', '=', r.loan_id.id)])
            if count > 1:
                raise ValidationError('Sorry, there is more than one installment on the same month.')


class LoanInput(models.Model):
    _name = 'employee.loan.payslip.input'
    _description = 'Loan Input in Payslip'

    @api.depends('payslip_id.number', 'loan_id')
    def _compute_payslip(self):
        for r in self:
            count = 1
            for i in r.loan_id.installment_ids.sorted(lambda obj: obj.date):
                if i == r.installment_id:
                    break
                count += 1
            r.name = (r.payslip_id.number or 'New slip') + '/' + r.loan_id.name + '/' + str(count)


    name = fields.Char('Description', compute=_compute_payslip, store=True)
    payslip_id = fields.Many2one('hr.payslip',string='Payslip', ondelete='cascade', required=True, readonly=True)
    payslip_employee_id = fields.Many2one(related='payslip_id.employee_id', readonly=True)
    payslip_date_from = fields.Date(related='payslip_id.date_from', readonly=True)
    payslip_date_to = fields.Date(related='payslip_id.date_to', readonly=True)
    loan_id = fields.Many2one(related='installment_id.loan_id', string="Loan")  # , required=True, domain=[('state', '=', 'closed'), ('payslip_status', '=', False)])
    installment_id = fields.Many2one('employee.loan.installment', "Installment", required=True, ondelete='cascade')
    loan_contract_id = fields.Many2one(related='installment_id.loan_id.contract_id', readonly=True)
    amount = fields.Monetary('Amount', currency_field='currency_id')
    total = fields.Monetary(related='installment_id.loan_id.amount', string='Loan Total', readonly=True)
    rule_id = fields.Many2one(related='installment_id.loan_id.rule_id', store=True)
    currency_id = fields.Many2one(related='installment_id.currency_id')
    balance=fields.Integer(string='Balance')
    @api.constrains('installment_id', 'payslip_id')
    def _validate_employee(self):
        for r in self:
            if not (r.loan_id and r.payslip_id):
                continue
            if r.loan_id.employee_id.id != r.payslip_id.employee_id.id:
                raise ValidationError('Loan "{}" does not belong to "{}"'.format(r.loan_id.name, r.payslip_id.employee_id.name))

    @api.constrains('amount', 'installment_id')
    def _validate_amount(self):
        for r in self:
            if not r.installment_id:
                continue
            if r.amount > r.installment_id.amount:
                raise ValidationError('Given amount ({}) can not be bigger than the installment ({})'
                                      .format(r.amount, r.installment_id.amount))
