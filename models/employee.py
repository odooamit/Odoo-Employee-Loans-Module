from openerp import api, fields, models, _
from openerp.exceptions import ValidationError


class Employee(models.Model):
    _inherit = 'hr.employee'

    loan_account_id = fields.Many2one('account.account', string='Loan Account',
                                      domain="[('company_id', '=', company_id )]")
    default_loan_display_account_id = fields.Many2one(related='company_id.loan_display_account_id', readonly=True)

    @api.constrains('loan_account_id')
    def _validate_loan_account(self):
        for r in self:
            if r.loan_account_id and r.loan_account_id.company_id != r.company_id:
                raise ValidationError('Loan account can not be from a different company!')
