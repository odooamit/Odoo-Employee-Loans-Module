from openerp import fields, api, models, _
from openerp.exceptions import UserError, AccessError, ValidationError, Warning


class Payment(models.Model):
    _inherit = 'account.payment'

    @api.one
    @api.depends('invoice_ids', 'payment_type', 'partner_type', 'partner_id')
    def _compute_destination_account_id(self):
        if not self.env.context.get('loan_payment'):
            return super(Payment, self)._compute_destination_account_id()

        employee = self.env['hr.employee'].search([('address_home_id', '=', self.partner_id.id)])
        if len(employee) != 1:
            raise ValidationError('Partner {} is related to more than one employee!\nI can not'
                                  ' determine which one has requested this loan.'.format(self.partner_id.name))
        elif len(employee) == 0:
            raise ValidationError('Sorry, I can find the employee for this partner: {}\nTo register a loan payment'
                                  'for an employee his/her partner (Home Address) must be set.'.format(self.partner_id.name))
        elif not employee.loan_account_id:
            raise ValidationError('Sorry, To register a loan payment for an employee his/her Loan account '
                                  'must be set.')
        self.destination_account_id = employee.loan_account_id.id

    @api.onchange('payment_type')
    def _onchange_payment_type(self):
        was_employee = False
        if self.partner_type == 'employee':
            was_employee = True

        res = super(Payment, self)._onchange_payment_type()

        if was_employee:
            self.partner_type = 'employee'
        return res

    def _get_counterpart_move_line_vals(self, invoice=False):
        res = super(Payment, self)._get_counterpart_move_line_vals(invoice=invoice)

        if self.partner_type == 'employee':
            name = 'Employee Payment'
            if self.env.context.get('loan_payment'):
                loan = self.env['employee.loan'].browse([self.env.context['loan_id']])
                loan_year = fields.Date.from_string(loan.date).year
                name += ': ' + str(loan_year) + '/' + loan.name
            res['name'] = name
        return res
