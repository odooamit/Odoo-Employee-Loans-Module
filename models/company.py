from openerp import api, fields, models, _
from openerp.exceptions import ValidationError


class Company(models.Model):
    _inherit = 'res.company'

    loan_max_amount = fields.Monetary(string='Loan Max Amount', currency_field='currency_id', default=5000.0)
    loan_display_account_id = fields.Many2one('account_display.account_display', string='Loan Display Account',
                                              domain="[('company_id', '=', id )]")
    loan_installment_max_percentage_from_wage = fields.Float('Maximum Loan Percentage',
                                                             digits=(5, 2),
                                                             help='This field will determine the maximum installment '
                                                                  'amount that can be deducted from an employee per '
                                                                  'month, based on his/her wage.\n'
                                                                  'For example, enter 50.0 to apply a percentage of 50%')
    short_loan_max_percentage_from_wage = fields.Float('Maximum Short Loan Percentage',
                                                       digits=(5, 2),
                                                       help='This field will determine the maximum amount of '
                                                            'short loan (e.g:advance salary) that an employee can'
                                                            ' request.')

    @api.constrains('loan_installment_max_percentage_from_wage')
    def _validate_loan_installment_max_percentage_from_wage(self):
        for r in self:
            if r.loan_installment_max_percentage_from_wage > 100:
                raise ValidationError('Sorry, Maximum Loan Percentage can not be larger than 100.00 .')
            if r.loan_installment_max_percentage_from_wage < 0:
                raise ValidationError('Sorry, Maximum Loan Percentage can not be less than 0 .')

    @api.constrains('short_loan_max_percentage_from_wage')
    def _validate_loan_installment_max_percentage_from_wage(self):
        for r in self:
            if r.loan_installment_max_percentage_from_wage > 100:
                raise ValidationError('Sorry, Maximum Short Loan Percentage can not be larger than 100.00 .')
            if r.loan_installment_max_percentage_from_wage < 0:
                raise ValidationError('Sorry, Maximum Short Loan Percentage can not be less than 0 .')

    @api.constrains('loan_display_account_id')
    def _validate_loan_display_account(self):
        for r in self:
            if r.loan_display_account_id and r.loan_display_account_id.company_id != r.id:
                raise ValidationError('Loan display account is for different company!\nCompany: {}'.format(r.name))
