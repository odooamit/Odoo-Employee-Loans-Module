# -*- coding:utf-8 -*-
from openerp import fields, models, api
from openerp.exceptions import ValidationError


class SalaryStructure(models.Model):
    _inherit = 'hr.payroll.structure'

    @api.multi
    def _compute_loan_rule(self):
        for r in self:
            loan_rule = r.rule_ids.filtered(lambda r: r.amount_select == 'loan')
            if len(loan_rule) == 1:
                r.loan_rule_id = loan_rule

    loan_rule_id = fields.Many2one('hr.salary.rule', 'Loans Rule', compute=_compute_loan_rule, readonly=True)


    @api.multi
    def validate_loan_rules(self, rules=False):
        self.ensure_one()
        # print('validating loan rules in salary structure')
        if not rules:
            rules = self.rule_ids
        loan_rules = rules.filtered(lambda r: r.amount_select == 'loan')
        if len(loan_rules) > 1:
            raise ValidationError('Sorry, there can not be more than one loan rule in {}.\n'
                                  'Loan rules : {}'.format(self.name, ','.join(loan_rules.mapped('name'))))

