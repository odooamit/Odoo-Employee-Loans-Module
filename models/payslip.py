#-*- coding:utf-8 -*-
from openerp import fields, api, models, _
from openerp.exceptions import ValidationError, UserError
from dateutil.relativedelta import relativedelta


class Payslip(models.Model):
    _inherit = 'hr.payslip'

    loan_input_line_ids = fields.One2many('employee.loan.payslip.input', 'payslip_id', string='Loan Deductions', copy=True)

    @api.multi
    def _get_loans_to_deduct(self):
        self.ensure_one()
        return self.env['employee.loan'].search([('employee_id', '=', self.employee_id.id),
                                                 '|', ('state', '=', 'running'), ('start_deductions', '=', True)])

    @api.multi
    def _get_installments(self, loan):
        self.ensure_one()
        if not (self.date_to and self.date_from):
            raise UserError('Please enter the period first.')

        installment = loan.installment_ids.filtered(lambda r: r.date <= self.date_to and r.payslip_status == False)

        return installment

    @api.multi
    def compute_sheet(self):
        for payslip in self:
            payslip.loan_input_line_ids = False
            loans = payslip._get_loans_to_deduct()  # contract_id is set by the super method
            if not loans:
                continue

            input_lines = payslip.loan_input_line_ids.browse([])
            for loan in loans:
                #_logger.info("!!!!!!!!!!!!!!!!! " + loan.name + " !!!!!!!!!!!!!!!!!!!!!!!")
                installments = payslip._get_installments(loan)
                for i in installments:
                    paid_installment= sum(i.loan_id.installment_ids.filtered(lambda r: r.payslip_status == True).mapped(
                        'amount') )or 0.0
                    input_lines += input_lines.new({
                        'installment_id': i.id,
                        'amount': i.amount,
                        'balance':  i.loan_id.amount -paid_installment
                    })

            payslip.loan_input_line_ids = input_lines

        return super(Payslip, self).compute_sheet()

    @api.multi
    def get_loan_lines_total(self, contracts):
        self.ensure_one()
        loan_lines_total = 0.0
        for contract in contracts:
            loan_line = self.details_by_salary_rule_category.filtered(
                lambda x: x.contract_id == contract and x.salary_rule_id == contract.struct_id.loan_rule_id)
            if len(loan_line) == 1:  # len(loan_line) should either be 1 or 0
                loan_lines_total += loan_line.total
        return loan_lines_total

    @api.multi
    def _validate_loan_input(self):
        for r in self:
            if r.contract_id:
                contracts = [r.contract_id]
            else:
                # get all valid contracts as they do when computing the payslip
                contracts = r.get_contract(r.employee_id, r.date_from, r.date_to)
                contracts = r.env['hr.contract'].browse(contracts)
            loan_lines_total = r.get_loan_lines_total(contracts)

            inputs_total = sum(r.loan_input_line_ids.mapped('amount') or [0.0])
            if inputs_total != loan_lines_total:
                operator = 'less' if inputs_total < loan_lines_total else 'more'
                raise ValidationError(
                    'Sorry, loan inputs total ({}) is {} than the computed value on Loan Salary Rule ({}).\n'
                    'Please recompute the sheet.'.format(inputs_total, operator, loan_lines_total))

            currencies = r.loan_input_line_ids.mapped('currency_id.name')
            if len(currencies) > 1:
                currencies_string = ', '.join(currencies)
                raise ValidationError('Loan installments can not have different currencies in the same payslip!\n'
                                      'Currencies are '+currencies_string+'.')

    @api.multi
    def submit_sheet(self):
        self._validate_loan_input()
        self.checkNetSalary()
        self.missed_loan_installment()
        super(Payslip, self).submit_sheet()

    @api.multi
    def get_salary_rule_credit_account_id(self, rule, employee):
        if rule.amount_select == 'loan':
            return employee and employee.loan_account_id.id or False
        else:
            return super(Payslip, self).get_salary_rule_credit_account_id(rule, employee)

    @api.multi
    def process_sheet(self):
        for r in self:
            for input_line in r.loan_input_line_ids:
                amount = input_line.amount
                if r.credit_note:
                    amount = -amount

                input_line.installment_id.amount_paid += amount
                if input_line.loan_id.balance == 0:
                    input_line.sudo().loan_id.payslip_status = True
                    input_line.loan_id.close_loan()

                loan_line = r.details_by_salary_rule_category.filtered(
                    lambda x: x.contract_id == input_line.loan_contract_id and x.salary_rule_id == input_line.rule_id)
                input_line.installment_id.payslip_line = loan_line
        return super(Payslip, self).process_sheet()

    def missed_loan_installment(self):
        if self.loan_input_line_ids:
            for loan in  self._get_loans_to_deduct():
                installment = self._get_installments(loan)
                if installment and not self.loan_input_line_ids.filtered(lambda r:r.loan_id == loan).mapped('installment_id'):
                    installment.date = fields.Date.from_string(
                        loan.installment_ids[len(loan.installment_ids) - 1].date) + relativedelta(months=+1)


    def create_back_installment(self,amount,loan_id):
        return loan_id.installment_ids.create({
            'name': str(loan_id.name) + '/' + str(len(loan_id.installment_ids)+1),
            'amount': amount,
            'date': fields.Date.from_string(loan_id.installment_ids[len(loan_id.installment_ids)-1].date)+relativedelta(months=+1),
            'payslip_status': False,
            'loan_id':loan_id.id
        })

    def refresh(self):
        loans = self._get_loans_to_deduct()
        if not loans:
            return

        input_lines = self.loan_input_line_ids.browse([])
        for loan in loans:
            installments = self._get_installments(loan)
            for i in installments:
                input_lines += input_lines.new({
                    'installment_id': i.id,
                    'amount': i.amount,
                    'balance': i.loan_id.amount - i.amount
                })

        self.loan_input_line_ids = input_lines
        return self.compute_sheet()

    @api.multi
    def checkNetSalary(self):
        sum=0
        net_salary = self.line_ids.filtered(lambda r: r.salary_rule_id.is_net_salary == True)
        if len(net_salary) > 1:
            raise UserError('There Is more Than One Rule Chosen As Net Salary')
        elif len(net_salary) < 1:
            raise UserError('Net Salary Not Detected , Please Choose One Rule As Net Salary')
        if net_salary.amount <0 :
            if self.loan_input_line_ids :
                a=self.loan_input_line_ids.mapped('amount')
                tem=0
                for su in a:
                   tem+=su

                if tem >abs(net_salary.amount):
                    for input_line in self.loan_input_line_ids:
                        if input_line.installment_id.amount > abs(net_salary.amount+sum) and net_salary.amount<0:
                            input_line.installment_id.amount-=abs(net_salary.amount)
                            sum+=abs(net_salary.amount)
                            self.create_back_installment(abs(net_salary.amount),input_line.loan_id)
                            net_salary = self.line_ids.filtered(lambda r: r.salary_rule_id.is_net_salary == True)
                    self.refresh()
                else:raise UserError("The Net Salary Is Negative For Employee {}" .format(self.employee_id.name))

            else:
                raise UserError("The Net Salary Is Negative For Employee {}" .format(self.employee_id.name))